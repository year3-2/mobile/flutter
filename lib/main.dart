import 'package:flutter/material.dart';
import 'package:helloflutterapp/main.dart';
void main()=>runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState()=> _HelloFlutterAppState();

}
String engGreeting = "Hello flutter";
String spnGreeting = "Hola flutter";
String thGreeting ="Sawasdee flutter";
class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText = engGreeting;

  @override
  Widget build(BuildContext context){

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("HELLO TEST"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed: (){setState(() {
              displayText = engGreeting;
            });}, icon: Icon(Icons.refresh)),
            IconButton(onPressed: (){setState(() {
              displayText = spnGreeting;

            });}, icon: Icon(Icons.translate)),
            IconButton(onPressed: (){setState(() {
              displayText = thGreeting;
            });}, icon: Icon(Icons.whatshot))
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 50),

          ),
        ),

      ),
    );
  }
  }


/*class HelloFlutterApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("HELLO TEST"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed: (){}, icon: Icon(Icons.refresh))
          ],
        ),
        body: Center(
          child: Text(
            "Hello flutter",
                style: TextStyle(fontSize: 50),
          ),
        ),

      ),
    );
  }
}*/

